package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.HasStyle;

import javax.annotation.Nullable;

/**
 * Mixin interface with CSS Flexbox features:<br>
 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
 *
 * @see CssFlexboxLayout
 */
public interface CssFlexboxAware extends HasStyle {

	/**
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	default void setAlignItems(@Nullable AlignItems alignItems) {
		setAlignItems(this, alignItems);
	}

	default void setDisplayFlex() {
		setDisplayFlex(this);
	}

	/**
	 * @param flexDirection nullable, removes if null
	 *
	 * @see FlexDirection
	 */
	default void setFlexDirection(@Nullable FlexDirection flexDirection) {
		setFlexDirection(this, flexDirection);
	}

	/**
	 * @param flexWrap nullable, removes if null
	 *
	 * @see FlexWrap
	 */
	default void setFlexWrap(@Nullable FlexWrap flexWrap) {
		setFlexWrap(this, flexWrap);
	}

	/**
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	default void setJustifyContent(@Nullable JustifyContent justifyContent) {
		setJustifyContent(this, justifyContent);
	}

	/**
	 * @param component  non-null
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	static void setAlignItems(HasStyle component, @Nullable AlignItems alignItems) {
		component.getStyle().set(CssFlexboxProperty.ALIGN_ITEMS.key(), alignItems != null ? alignItems.value() : null);
	}

	static void setDisplayFlex(HasStyle component) {
		component.getStyle().set("display", "flex");
	}

	/**
	 * @param component     non-null
	 * @param flexDirection nullable, removes if null
	 *
	 * @see FlexDirection
	 */
	static void setFlexDirection(HasStyle component, @Nullable FlexDirection flexDirection) {
		component.getStyle().set(CssFlexboxProperty.FLEX_DIRECTION.key(), flexDirection != null ? flexDirection.value() : null);
	}

	/**
	 * @param component non-null
	 * @param flexWrap  nullable, removes if null
	 *
	 * @see FlexWrap
	 */
	static void setFlexWrap(HasStyle component, @Nullable FlexWrap flexWrap) {
		component.getStyle().set(CssFlexboxProperty.FLEX_WRAP.key(), flexWrap != null ? flexWrap.value() : null);
	}

	/**
	 * @param component      non-null
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	static void setJustifyContent(HasStyle component, @Nullable JustifyContent justifyContent) {
		component.getStyle().set(CssFlexboxProperty.JUSTIFY_CONTENT.key(), justifyContent != null ? justifyContent.value() : null);
	}
}
