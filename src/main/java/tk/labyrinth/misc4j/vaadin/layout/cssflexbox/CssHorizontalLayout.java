package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#ROW}).<br>
 * Note: this property may be altered later, even to "column" values.<br>
 */
public class CssHorizontalLayout extends CssFlexboxLayout {

	public CssHorizontalLayout() {
		super(FlexDirection.ROW);
	}
}
