package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a><br>
 *
 * @see CssGridAware
 * @see CssGridItem
 * @see CssGridLayout
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum CssGridProperty {
	ALIGN_CONTENT("align-content"),
	ALIGN_ITEMS("align-items"),
	COLUMN_GAP("column-gap"),
	GAP("gap"),
	GRID_AREA("grid-area"),
	GRID_COLUMN("grid-column"),
	GRID_COLUMN_END("grid-column-end"),
	GRID_COLUMN_START("grid-column-start"),
	GRID_TEMPLATE_AREAS("grid-template-areas"),
	GRID_TEMPLATE_COLUMNS("grid-template-columns"),
	GRID_TEMPLATE_ROWS("grid-template-rows"),
	JUSTIFY_CONTENT("justify-content"),
	JUSTIFY_ITEMS("justify-items"),
	ROW_GAP("row-gap");

	private final String key;
}
