package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;

/**
 * Flexible layout based on CSS Flexbox.<br>
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
 *
 * @see CssFlexboxAware
 */
@Tag(Tag.DIV)
public class CssFlexboxLayout extends Component implements CssFlexboxAware, HasComponents, HasSize {

	{
		setDisplayFlex();
	}

	public CssFlexboxLayout() {
		super();
	}

	public CssFlexboxLayout(FlexDirection flexDirection) {
		this();
		setFlexDirection(flexDirection);
	}
}
