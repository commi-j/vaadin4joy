package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide (scrolling required):<br>
 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
 * Image:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/10/justify-content.svg" alt=""><br>
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum JustifyContent {
	CENTER("center"),
	FLEX_END("flex-end"),
	FLEX_START("flex-start"),
	SPACE_AROUND("space-around"),
	SPACE_BETWEEN("space-between"),
	SPACE_EVENLY("space-evenly");

	private final String value;
}
