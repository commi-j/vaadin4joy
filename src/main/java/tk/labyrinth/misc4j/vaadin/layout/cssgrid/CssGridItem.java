package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import com.vaadin.flow.component.HasStyle;

import javax.annotation.Nullable;

public interface CssGridItem {

	/**
	 * @param component  non-null
	 * @param gridColumn nullable, removes if null
	 */
	static void setGridColumn(HasStyle component, @Nullable String gridColumn) {
		component.getStyle().set(CssGridProperty.GRID_COLUMN.key(), gridColumn);
	}

	/**
	 * @param component     non-null
	 * @param gridColumnEnd nullable, removes if null
	 */
	static void setGridColumnEnd(HasStyle component, @Nullable String gridColumnEnd) {
		component.getStyle().set(CssGridProperty.GRID_COLUMN_END.key(), gridColumnEnd);
	}

	/**
	 * @param component      non-null
	 * @param gridColumnSpan nullable, removes if null
	 */
	static void setGridColumnSpan(HasStyle component, @Nullable Integer gridColumnSpan) {
		setGridColumn(component, gridColumnSpan != null ? "span " + gridColumnSpan : null);
	}

	/**
	 * @param component       non-null
	 * @param gridColumnStart nullable, removes if null
	 */
	static void setGridColumnStart(HasStyle component, @Nullable String gridColumnStart) {
		component.getStyle().set(CssGridProperty.GRID_COLUMN_START.key(), gridColumnStart);
	}
}
