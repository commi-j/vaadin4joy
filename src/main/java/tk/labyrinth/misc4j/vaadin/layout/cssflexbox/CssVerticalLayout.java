package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#COLUMN}).<br>
 * Note: this property may be altered later, even to "row" values.<br>
 */
public class CssVerticalLayout extends CssFlexboxLayout {

	public CssVerticalLayout() {
		super(FlexDirection.COLUMN);
	}
}
