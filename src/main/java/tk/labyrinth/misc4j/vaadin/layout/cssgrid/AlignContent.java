package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
 * Start:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-start.svg" alt=""><br>
 * End:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-end.svg" alt=""><br>
 * Center:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-center.svg" alt=""><br>
 * Stretch:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-stretch.svg" alt=""><br>
 * Space-around:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-around.svg" alt=""><br>
 * Space-between:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-between.svg" alt=""><br>
 * Space-evenly:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-evenly.svg" alt=""><br>
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum AlignContent {
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-center.svg" alt=""><br>
	 */
	CENTER("center"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-end.svg" alt=""><br>
	 */
	END("end"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-around.svg" alt=""><br>
	 */
	SPACE_AROUND("space-around"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-between.svg" alt=""><br>
	 */
	SPACE_BETWEEN("space-between"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-space-evenly.svg" alt=""><br>
	 */
	SPACE_EVENLY("space-evenly"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-start.svg" alt=""><br>
	 */
	START("start"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-stretch.svg" alt=""><br>
	 */
	STRETCH("stretch");

	private final String value;
}
