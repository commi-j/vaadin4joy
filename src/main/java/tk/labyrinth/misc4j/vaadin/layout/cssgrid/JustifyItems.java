package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
 * Start:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-start.svg" alt=""><br>
 * End:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-end.svg" alt=""><br>
 * Center:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-center.svg" alt=""><br>
 * Stretch:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-stretch.svg" alt=""><br>
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum JustifyItems {
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-center.svg" alt=""><br>
	 */
	CENTER("center"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-end.svg" alt=""><br>
	 */
	END("end"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-start.svg" alt=""><br>
	 */
	START("start"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-stretch.svg" alt=""><br>
	 */
	STRETCH("stretch");

	private final String value;
}
