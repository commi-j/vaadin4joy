package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-items</a><br>
 * Start:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-items-start.svg" alt=""><br>
 * End:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-items-end.svg" alt=""><br>
 * Center:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-items-center.svg" alt=""><br>
 * Stretch:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-items-stretch.svg" alt=""><br>
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum AlignItems {
	CENTER("center"),
	END("end"),
	START("start"),
	STRETCH("stretch");

	private final String value;
}
