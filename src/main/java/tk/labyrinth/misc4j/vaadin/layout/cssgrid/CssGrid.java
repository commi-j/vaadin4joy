package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Constants for {@link CssGridAware}.
 *
 * @see CssGridAware
 * @see CssGridLayout
 */
@Getter
@RequiredArgsConstructor
public enum CssGrid {
	ALIGN_ITEMS("align-items"),
	AREA("grid-area"),
	JUSTIFY_ITEMS("justify-items"),
	TEMPLATE_AREAS("grid-template-areas"),
	TEMPLATE_COLUMNS("grid-template-columns"),
	TEMPLATE_ROWS("grid-template-rows");

	private final String key;
}
