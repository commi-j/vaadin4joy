package tk.labyrinth.misc4j.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.HasStyle;

import javax.annotation.Nullable;

public interface CssFlexItem {

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg" alt=""><br>
	 *
	 * @param component non-null
	 * @param flexGrow  nullable, removes if null
	 */
	static void setFlexGrow(HasStyle component, double flexGrow) {
		setFlexGrow(component, Double.valueOf(flexGrow));
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg" alt=""><br>
	 *
	 * @param component non-null
	 * @param flexGrow  nullable, removes if null
	 */
	static void setFlexGrow(HasStyle component, @Nullable Double flexGrow) {
		component.getStyle().set(CssFlexboxProperty.FLEX_GROW.key(), flexGrow != null ? flexGrow.toString() : null);
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 *
	 * @param component  non-null
	 * @param flexShrink nullable, removes if null
	 */
	static void setFlexShrink(HasStyle component, double flexShrink) {
		setFlexShrink(component, Double.valueOf(flexShrink));
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 *
	 * @param component  non-null
	 * @param flexShrink nullable, removes if null
	 */
	static void setFlexShrink(HasStyle component, @Nullable Double flexShrink) {
		component.getStyle().set(CssFlexboxProperty.FLEX_SHRINK.key(), flexShrink != null ? flexShrink.toString() : null);
	}
}
