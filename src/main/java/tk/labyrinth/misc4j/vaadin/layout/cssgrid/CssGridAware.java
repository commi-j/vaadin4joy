package tk.labyrinth.misc4j.vaadin.layout.cssgrid;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;

import javax.annotation.Nullable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Mixin interface with CSS Grid features:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a><br>
 * <br>
 * Note: This one may add "grid-area" style property on children but does not remove it on child removal.<br>
 *
 * @see CssGridLayout
 */
public interface CssGridAware extends HasComponents, HasStyle {

	default void add(Component component, String gridArea) {
		component.getElement().getStyle().set(CssGridProperty.GRID_AREA.key(), gridArea);
		add(component);
	}

	/**
	 * @param alignContent nullable, removes if null
	 *
	 * @see AlignContent
	 */
	default void setAlignContent(@Nullable AlignContent alignContent) {
		setAlignContent(this, alignContent);
	}

	/**
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	default void setAlignItems(@Nullable AlignItems alignItems) {
		setAlignItems(this, alignItems);
	}

	default void setDisplayGrid() {
		setDisplayGrid(this);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param gap nullable, removes if null
	 */
	default void setGap(@Nullable String gap) {
		setGap(this, gap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param columnGap nullable, removes if null
	 * @param rowGap    nullable, removes if null
	 */
	default void setGap(@Nullable String columnGap, @Nullable String rowGap) {
		setGap(this, columnGap, rowGap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt=""><br>
	 *
	 * @param gridTemplateAreas nullable, removes if null
	 */
	default void setGridTemplateAreas(@Nullable String... gridTemplateAreas) {
		setGridTemplateAreas(this, gridTemplateAreas);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param gridTemplateColumns nullable, removes if null
	 */
	default void setGridTemplateColumns(@Nullable String gridTemplateColumns) {
		setGridTemplateColumns(this, gridTemplateColumns);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param gridTemplateRows nullable, removes if null
	 */
	default void setGridTemplateRows(@Nullable String gridTemplateRows) {
		setGridTemplateRows(this, gridTemplateRows);
	}

	/**
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	default void setJustifyContent(@Nullable JustifyContent justifyContent) {
		setJustifyContent(this, justifyContent);
	}

	/**
	 * @param justifyItems nullable, removes if null
	 *
	 * @see JustifyItems
	 */
	default void setJustifyItems(@Nullable JustifyItems justifyItems) {
		setJustifyItems(this, justifyItems);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param rowGap nullable, removes if null
	 */
	default void setRowGap(@Nullable String rowGap) {
		setRowGap(this, rowGap);
	}

	/**
	 * @param component    non-null
	 * @param alignContent nullable, removes if null
	 *
	 * @see AlignContent
	 */
	static void setAlignContent(HasStyle component, @Nullable AlignContent alignContent) {
		component.getStyle().set(CssGridProperty.ALIGN_CONTENT.key(), alignContent != null ? alignContent.value() : null);
	}

	/**
	 * @param component  non-null
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	static void setAlignItems(HasStyle component, @Nullable AlignItems alignItems) {
		component.getStyle().set(CssGridProperty.ALIGN_ITEMS.key(), alignItems != null ? alignItems.value() : null);
	}

	static void setDisplayGrid(HasStyle component) {
		component.getStyle().set("display", "grid");
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param component non-null
	 * @param gap       nullable, removes if null
	 */
	static void setGap(HasStyle component, @Nullable String gap) {
		component.getStyle().set(CssGridProperty.GAP.key(), gap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param component non-null
	 * @param columnGap nullable, removes if null
	 * @param rowGap    nullable, removes if null
	 */
	static void setGap(HasStyle component, @Nullable String columnGap, @Nullable String rowGap) {
		component.getStyle().set(CssGridProperty.COLUMN_GAP.key(), columnGap);
		component.getStyle().set(CssGridProperty.ROW_GAP.key(), rowGap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt=""><br>
	 *
	 * @param component         non-null
	 * @param gridTemplateAreas nullable, removes if null
	 */
	static void setGridTemplateAreas(HasStyle component, @Nullable String... gridTemplateAreas) {
		component.getStyle().set(CssGridProperty.GRID_TEMPLATE_AREAS.key(), gridTemplateAreas != null
				? Stream.of(gridTemplateAreas).map(areas -> "\"" + areas + "\"").collect(Collectors.joining(" "))
				: null);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param component           non-null
	 * @param gridTemplateColumns nullable, removes if null
	 */
	static void setGridTemplateColumns(HasStyle component, @Nullable String gridTemplateColumns) {
		component.getStyle().set(CssGridProperty.GRID_TEMPLATE_COLUMNS.key(), gridTemplateColumns);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param component        non-null
	 * @param gridTemplateRows nullable, removes if null
	 */
	static void setGridTemplateRows(HasStyle component, @Nullable String gridTemplateRows) {
		component.getStyle().set(CssGridProperty.GRID_TEMPLATE_ROWS.key(), gridTemplateRows);
	}

	/**
	 * @param component      non-null
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	static void setJustifyContent(HasStyle component, @Nullable JustifyContent justifyContent) {
		component.getStyle().set(CssGridProperty.JUSTIFY_CONTENT.key(), justifyContent != null ? justifyContent.value() : null);
	}

	/**
	 * @param component    non-null
	 * @param justifyItems nullable, removes if null
	 *
	 * @see JustifyItems
	 */
	static void setJustifyItems(HasStyle component, @Nullable JustifyItems justifyItems) {
		component.getStyle().set(CssGridProperty.JUSTIFY_ITEMS.key(), justifyItems != null ? justifyItems.value() : null);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt=""><br>
	 *
	 * @param component non-null
	 * @param rowGap    nullable, removes if null
	 */
	static void setRowGap(HasStyle component, @Nullable String rowGap) {
		component.getStyle().set(CssGridProperty.ROW_GAP.key(), rowGap);
	}
}
