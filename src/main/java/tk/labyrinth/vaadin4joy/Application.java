package tk.labyrinth.vaadin4joy;

import com.vaadin.flow.spring.annotation.EnableVaadin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableVaadin("tk.labyrinth.vaadin4joy")
@SpringBootApplication
public class Application {

	public static void main(String... args) {
		SpringApplication.run(Application.class);
	}
}
