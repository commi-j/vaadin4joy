package tk.labyrinth.vaadin4joy.front.page;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.JustifyItems;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.util.stream.Stream;

@Route("yoficator")
public class YoficatorPage extends CssGridLayout implements HasUrlParameter<String> {

	private final TextArea inputArea = new TextArea("Input");

	private final TextArea outputArea = new TextArea("Output");

	private void configureGrid() {
		addClassName("yoficator");
		setGridTemplateAreas(
				". . .",
				"header header header",
				"inputArea processButton outputArea",
				"footer footer footer",
				". . .");
		setGridTemplateColumns("1fr auto 1fr");
		setGridTemplateRows("50px auto 1fr auto 50px");
		setHeightFull();
		setAlignItems(AlignItems.CENTER);
		setJustifyItems(JustifyItems.CENTER);
	}

	private void initialize(@Nullable String value) {
		UI.getCurrent().getPage().setTitle("Ёficator");
		inputArea.setValue(value != null ? value : "Съешь ещё этих мягких французских булок, да выпей же чаю!");
	}

	private void populateGrid() {
		{
			Anchor inspiredByAnchor = new Anchor("https://pikabu.ru/story/yoyo_6631749", "Ёё");
			inspiredByAnchor.setTarget("_blank");
			add(new HorizontalLayout(new Label("Inspired By:"), inspiredByAnchor), "header");
		}
		{
			add(inputArea, "inputArea");
			add(new Button("Ёficate", event -> yoficate()), "processButton");
			add(outputArea, "outputArea");
			Stream.of(inputArea, outputArea).forEach(area -> {
				area.setWidth("80%");
				area.setHeight("80%");
			});
		}
		{
			Anchor sourceCodeAnchor = new Anchor("https://gitlab.com/commitman/vaadin4joy", "vaadin4joy");
			sourceCodeAnchor.setTarget("_blank");
			Anchor vaadinAnchor = new Anchor("https://vaadin.com/", VaadinIcon.VAADIN_V.create());
			vaadinAnchor.setTarget("_blank");
			add(new HorizontalLayout(new Label("Source Code:"), sourceCodeAnchor, new Label("Powered By:"), vaadinAnchor), "footer");
		}
	}

	@PostConstruct
	private void postConstruct() {
	}

	private void yoficate() {
		outputArea.setValue(yoficate(inputArea.getValue()));
	}

	@Override
	public void setParameter(BeforeEvent event, @WildcardParameter String parameter) {
		configureGrid();
		populateGrid();
		initialize(!parameter.isEmpty() ? parameter : null);
	}

	private static String yoficate(String input) {
		return input
				.replaceAll("[АЕИОУЫЭЮЯ]", "Ё")
				.replaceAll("[аеиоуыэюя]", "ё");
	}
}
