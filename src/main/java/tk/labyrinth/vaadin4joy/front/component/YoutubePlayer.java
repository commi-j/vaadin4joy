package tk.labyrinth.vaadin4joy.front.component;

import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.dom.Element;
import tk.labyrinth.vaadin4joy.common.util.YoutubeUrlUtils;

import javax.annotation.Nullable;

@Tag("iframe")
public class YoutubePlayer extends HtmlComponent {

	private void setAttribute(String attribute, @Nullable String value) {
		Element element = getElement();
		if (value != null) {
			element.setAttribute(attribute, value);
		} else {
			element.removeAttribute(attribute);
		}
	}

	public void setAllowFullscreen(boolean allowFullscreen) {
		getElement().setAttribute("allowfullscreen", allowFullscreen);
	}

	public void setVideoId(@Nullable String videoId) {
		setAttribute("src", videoId != null ? YoutubeUrlUtils.videoEmbedded(videoId) : null);
	}
}
