package tk.labyrinth.vaadin4joy.common.util;

public class YoutubeUrlUtils {

	public static String thumbnailDefault(String videoId) {
		return "https://img.youtube.com/vi/" + videoId + "/default.jpg";
	}

	public static String videoEmbedded(String videoId) {
		return "https://www.youtube.com/embed/" + videoId;
	}
}
