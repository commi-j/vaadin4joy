package tk.labyrinth.vaadin4joy.opertv.common.catalogue;

import tk.labyrinth.vaadin4joy.opertv.common.model.video.Tag;

public interface TagCatalogue {

	Tag getTag(String operId);

	void storeTag(Tag tag);
}
