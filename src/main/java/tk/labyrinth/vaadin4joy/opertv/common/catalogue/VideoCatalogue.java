package tk.labyrinth.vaadin4joy.opertv.common.catalogue;

import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Stream;

public interface VideoCatalogue {

	@Nullable
	Video getCurrent(int operId);

	List<Video> getPlaylist(int operId);

	void storeVideos(Stream<Video> videos);
}
