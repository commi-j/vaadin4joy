package tk.labyrinth.vaadin4joy.opertv.common.model.video;

public enum TagCategory {
	GUEST,
	HOST,
	PROJECT,
	TIMELINE,
	TOPIC,
}
