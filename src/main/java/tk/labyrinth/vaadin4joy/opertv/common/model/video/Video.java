package tk.labyrinth.vaadin4joy.opertv.common.model.video;

import lombok.Value;

import java.time.LocalDate;
import java.util.List;

@Value
public class Video {

	Integer operId;

	LocalDate releasedAt;

	List<Tag> tags;

	String title;

	String youtubeId;

	public String buildOperUrl() {
		return "https://oper.ru/video/view.php?t=" + operId;
	}
}
