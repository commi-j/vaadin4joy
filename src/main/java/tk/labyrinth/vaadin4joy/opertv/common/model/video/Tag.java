package tk.labyrinth.vaadin4joy.opertv.common.model.video;

import lombok.Value;

@Value
public class Tag {

	TagCategory category;

	String name;

	String operId;

	Tag parent;
}
