package tk.labyrinth.vaadin4joy.opertv.back;

import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;
import tk.labyrinth.vaadin4joy.opertv.back.external.OperConnector;
import tk.labyrinth.vaadin4joy.opertv.back.external.OperMerger;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class OperTvModule {

	private final OperConnector operConnector;

	private final OperMerger operMerger;

	@PostConstruct
	private void postConstruct() {
		operMerger.merge(operConnector.getForMonth(2019, 4).stream());
		operMerger.merge(operConnector.getForMonth(2019, 5).stream());
	}

	@Deprecated
	private static int getLatestVideoId() {
		try {
			Document document = Jsoup.connect("https://oper.ru/video").get();
			Element latestVideoAnchor = document.selectFirst("ins.thumbnail > div.r > div > a");
			String latestVideoHref = latestVideoAnchor.attr("href");
			return Integer.parseInt(latestVideoHref.substring(latestVideoHref.indexOf("t=") + 2));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
