package tk.labyrinth.vaadin4joy.opertv.back.model.video.oper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Slf4j
@Value
public class OperVideo {

	LocalDate dt;

	List<OperGuest> guests;

	@JsonProperty("id_ext")
	Integer idExt;

	String name;

	List<OperTag> tags;

	List<Map<String, Object>> titles;

	@JsonProperty("url_youtube")
	String urlYoutube;

	public String buildYoutubeId() {
		String idPrefix = "v=";
		int idIndex = urlYoutube.indexOf(idPrefix);
		if (idIndex == -1) {
			logger.warn("Suspicious urlYoutube = {}, idExt = {}", urlYoutube, idExt);
		}
		return urlYoutube.substring(idIndex + idPrefix.length());
	}
}
