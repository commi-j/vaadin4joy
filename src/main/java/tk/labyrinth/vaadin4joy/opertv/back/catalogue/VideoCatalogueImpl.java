package tk.labyrinth.vaadin4joy.opertv.back.catalogue;

import lombok.Value;
import org.springframework.stereotype.Component;
import tk.labyrinth.vaadin4joy.opertv.common.catalogue.VideoCatalogue;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class VideoCatalogueImpl implements VideoCatalogue {

	private final int depth = 20;

	private final ConcurrentMap<Integer, Video> videosByOperId = new ConcurrentHashMap<>();

	private final ConcurrentMap<NavigationKey, Video[]> videosByNavigationKey = new ConcurrentHashMap<>();

	private volatile List<Video> videosByReleasedAt = new ArrayList<>();

	@Override
	public Video getCurrent(int operId) {
		return videosByOperId.get(operId);
	}

	@Override
	public List<Video> getPlaylist(int operId) {
		List<Video> result;
		{
			Video video = getCurrent(operId);
			if (video != null) {
				List<Video> videoList = videosByReleasedAt;
				int from;
				int to;
				{
					int index = videoList.indexOf(video);
					int potentialFrom = Math.max(index - depth, 0);
					to = Math.min(potentialFrom + (depth * 2 + 1), videoList.size());
					from = Math.max(to - (depth * 2 + 1), 0);
				}
				{
					result = new ArrayList<>();
					for (int i = from; i < to; i++) {
						result.add(videoList.get(i));
					}
				}
			} else {
				result = Collections.emptyList();
			}
		}
		return result;
	}

	@Override
	public void storeVideos(Stream<Video> videos) {
		videos.forEach(video -> videosByOperId.put(video.getOperId(), video));
		videosByReleasedAt = videosByOperId.values().stream().sorted(Comparator.comparing(video ->
				video.getReleasedAt() + "#" + video.getOperId())).collect(Collectors.toList());
		videosByNavigationKey.clear();
	}

	@Value
	private static class NavigationKey {

		boolean forward;

		String tagOperId;

		int videoOperId;
	}
}
