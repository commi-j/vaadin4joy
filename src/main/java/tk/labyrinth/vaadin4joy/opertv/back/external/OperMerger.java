package tk.labyrinth.vaadin4joy.opertv.back.external;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tk.labyrinth.vaadin4joy.opertv.back.model.video.oper.OperVideo;
import tk.labyrinth.vaadin4joy.opertv.common.catalogue.TagCatalogue;
import tk.labyrinth.vaadin4joy.opertv.common.catalogue.VideoCatalogue;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Tag;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class OperMerger {

	private final TagCatalogue tagCatalogue;

	private final VideoCatalogue videoCatalogue;

	private Video buildVideo(OperVideo operVideo) {
		return new Video(
				operVideo.getIdExt(),
				operVideo.getDt(),
				mergeTags(operVideo),
				operVideo.getName(),
				operVideo.buildYoutubeId()
		);
	}

	private List<Tag> mergeTags(OperVideo operVideo) {
		List<Tag> result = new ArrayList<>();
		operVideo.getGuests().forEach(operGuest -> result.add(new Tag(
				null, operGuest.getName(), operGuest.buildOperId(), null)));
		operVideo.getTags().forEach(operTag -> result.add(new Tag(
				null, operTag.getName(), operTag.buildOperId(), null)));
		operVideo.getTitles().forEach(operTitle -> {
			System.out.println("WAT: " + operVideo.getIdExt());
			// TODO
		});
		result.forEach(tagCatalogue::storeTag);
		return Collections.unmodifiableList(result);
	}

	public void merge(Stream<OperVideo> operVideos) {
		videoCatalogue.storeVideos(operVideos.map(this::buildVideo));
	}
}
