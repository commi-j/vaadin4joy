package tk.labyrinth.vaadin4joy.opertv.back.catalogue;

import org.springframework.stereotype.Component;
import tk.labyrinth.vaadin4joy.opertv.common.catalogue.TagCatalogue;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Tag;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class TagCatalogueImpl implements TagCatalogue {

	private final ConcurrentMap<String, Tag> tagsByOperId = new ConcurrentHashMap<>();

	@Override
	public Tag getTag(String operId) {
		return tagsByOperId.get(operId);
	}

	@Override
	public void storeTag(Tag tag) {
		tagsByOperId.put(tag.getOperId(), tag);
	}
}
