package tk.labyrinth.vaadin4joy.opertv.back.model.video.oper;

import lombok.Value;

@Value
public class OperGuest {

	Integer id;

	String name;

	public String buildOperId() {
		return "guest_" + id;
	}
}
