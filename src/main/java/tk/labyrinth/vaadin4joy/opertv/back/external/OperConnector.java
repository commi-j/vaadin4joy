package tk.labyrinth.vaadin4joy.opertv.back.external;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.labyrinth.vaadin4joy.opertv.back.model.video.oper.OperVideo;
import tk.labyrinth.vaadin4joy.opertv.back.model.video.oper.OperVideos;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class OperConnector {

	private final ObjectMapper objectMapper;

	public List<OperVideo> getForMonth(int year, int month) {
		OperVideos operVideos;
		try {
			LocalDate from = LocalDate.of(year, month, 1);
			LocalDate to = from.plusMonths(1).minusDays(1);
			RestTemplate rt = new RestTemplate();
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("from", from);
			parameters.put("to", to);
			String response = rt.getForObject("https://catalog.oper.ru/b/search?dts={from}&dte={to}",
					String.class, parameters);
			operVideos = objectMapper.readValue(response, OperVideos.class);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
		return operVideos.getVideos();
	}

	public static Document fetch(String url) {
		try {
			return Jsoup.connect(url).get();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}

