package tk.labyrinth.vaadin4joy.opertv.back.model.video.oper;

import lombok.Data;

import java.util.List;

@Data
public class OperVideos {

	private List<OperVideo> videos;
}
