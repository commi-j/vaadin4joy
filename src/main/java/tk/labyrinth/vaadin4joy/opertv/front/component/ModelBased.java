package tk.labyrinth.vaadin4joy.opertv.front.component;

import java.util.function.UnaryOperator;

public interface ModelBased<M> {

	M getModel();

	void setModel(M model);

	default void updateModel(UnaryOperator<M> operator) {
		setModel(operator.apply(getModel()));
	}
}
