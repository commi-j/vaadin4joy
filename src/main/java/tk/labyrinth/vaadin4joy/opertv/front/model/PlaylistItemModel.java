package tk.labyrinth.vaadin4joy.opertv.front.model;

import lombok.Value;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;

@Value
public class PlaylistItemModel {

	boolean selected;

	Video video;
}
