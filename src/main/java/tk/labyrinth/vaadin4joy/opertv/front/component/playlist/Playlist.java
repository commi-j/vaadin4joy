package tk.labyrinth.vaadin4joy.opertv.front.component.playlist;

import com.vaadin.flow.component.Composite;
import tk.labyrinth.misc4j.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.misc4j.vaadin.layout.cssflexbox.CssFlexboxLayout;
import tk.labyrinth.misc4j.vaadin.layout.cssflexbox.FlexDirection;
import tk.labyrinth.misc4j.vaadin.layout.cssflexbox.JustifyContent;
import tk.labyrinth.vaadin4joy.opertv.front.component.ModelBased;
import tk.labyrinth.vaadin4joy.opertv.front.model.PlaylistItemModel;
import tk.labyrinth.vaadin4joy.opertv.front.model.PlaylistModel;

import java.util.Collections;
import java.util.Objects;

public class Playlist extends Composite<CssFlexboxLayout> implements ModelBased<PlaylistModel> {

	private PlaylistModel model = new PlaylistModel(null, Collections.emptyList());

	@Override
	protected CssFlexboxLayout initContent() {
		CssFlexboxLayout result = new CssFlexboxLayout(FlexDirection.ROW);
		result.addClassName("playlist");
		result.setAlignItems(AlignItems.CENTER);
		result.setJustifyContent(JustifyContent.CENTER);
		return result;
	}

	@Override
	public PlaylistModel getModel() {
		return model;
	}

	@Override
	public void setModel(PlaylistModel model) {
		this.model = model;
		getContent().removeAll();
		model.getVideos().forEach(video -> {
			PlaylistItem item = new PlaylistItem();
			item.setModel(new PlaylistItemModel(Objects.equals(video.getOperId(), model.getSelectedOperId()), video));
			getContent().add(item);
		});
	}
}
