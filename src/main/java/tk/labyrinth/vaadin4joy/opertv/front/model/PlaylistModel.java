package tk.labyrinth.vaadin4joy.opertv.front.model;

import lombok.Value;
import lombok.experimental.Wither;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;

import javax.annotation.Nullable;
import java.util.List;

@Value
@Wither
public class PlaylistModel {

	@Nullable
	Integer selectedOperId;

	List<Video> videos;
}
