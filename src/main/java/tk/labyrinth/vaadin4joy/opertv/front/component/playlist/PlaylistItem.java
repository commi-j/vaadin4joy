package tk.labyrinth.vaadin4joy.opertv.front.component.playlist;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.RouterLink;
import tk.labyrinth.vaadin4joy.common.util.YoutubeUrlUtils;
import tk.labyrinth.vaadin4joy.opertv.front.component.ModelBased;
import tk.labyrinth.vaadin4joy.opertv.front.model.PlaylistItemModel;
import tk.labyrinth.vaadin4joy.opertv.front.page.OperTvPage;

public class PlaylistItem extends Composite<RouterLink> implements ModelBased<PlaylistItemModel> {

	private final Image image = new Image();

	private PlaylistItemModel model = new PlaylistItemModel(false, null);

	@Override
	protected RouterLink initContent() {
		RouterLink content = super.initContent();
		{
			image.getStyle().set("display", "block");
			content.add(image);
		}
		content.addClassName("playlist-item");
		return content;
	}

	@Override
	public PlaylistItemModel getModel() {
		return model;
	}

	@Override
	public void setModel(PlaylistItemModel model) {
		this.model = model;
		image.setSrc(YoutubeUrlUtils.thumbnailDefault(model.getVideo().getYoutubeId()));
		getContent().setClassName("selected", model.isSelected());
		getContent().setRoute(UI.getCurrent().getRouter(), OperTvPage.class, model.getVideo().getOperId().toString());
	}
}
