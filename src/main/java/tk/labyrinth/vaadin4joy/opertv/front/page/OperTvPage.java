package tk.labyrinth.vaadin4joy.opertv.front.page;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.misc4j.vaadin.layout.cssgrid.JustifyItems;
import tk.labyrinth.vaadin4joy.front.component.YoutubePlayer;
import tk.labyrinth.vaadin4joy.opertv.common.catalogue.VideoCatalogue;
import tk.labyrinth.vaadin4joy.opertv.common.model.video.Video;
import tk.labyrinth.vaadin4joy.opertv.front.component.playlist.Playlist;
import tk.labyrinth.vaadin4joy.opertv.front.model.PlaylistModel;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.util.Collections;

@RequiredArgsConstructor
@Route("opertv")
@StyleSheet("css/default.css")
public class OperTvPage extends Composite<CssGridLayout> implements HasUrlParameter<String> {

	private final VideoCatalogue catalogue;

	private final YoutubePlayer player = new YoutubePlayer();

	private final Playlist playlist = new Playlist();

	private void configureGrid() {
		getContent().setAlignItems(AlignItems.CENTER);
		getContent().setHeightFull();
		getContent().setJustifyItems(JustifyItems.CENTER);
		getContent().setGridTemplateAreas("player", "playlist");
		getContent().setGridTemplateRows("5fr 1fr");
	}

	private void populateGrid() {
		{
			player.setAllowFullscreen(true);
			player.setWidth("1280px");
			player.setHeight("720px");
			getContent().add(player, "player");
		}
		{
			playlist.getContent().setWidthFull();
			playlist.getContent().setHeightFull();
			getContent().add(playlist, "playlist");
		}
	}

	@PostConstruct
	private void postConstruct() {
		configureGrid();
		populateGrid();
	}

	@Override
	public void setParameter(BeforeEvent event, @WildcardParameter String parameter) {
		Integer videoOperId = resolveVideoOperId(parameter);
		Video video = videoOperId != null ? catalogue.getCurrent(videoOperId) : null;
		player.setVideoId(video != null ? video.getYoutubeId() : null);
		playlist.setModel(new PlaylistModel(videoOperId, videoOperId != null ? catalogue.getPlaylist(videoOperId) : Collections.emptyList()));
	}

	@Nullable
	private static Integer resolveVideoOperId(String parameter) {
		Integer result;
		if (!parameter.isEmpty()) {
			try {
				result = Integer.parseInt(parameter);
			} catch (NumberFormatException ex) {
				result = null;
			}
		} else {
			result = null;
		}
		return result;
	}
}
