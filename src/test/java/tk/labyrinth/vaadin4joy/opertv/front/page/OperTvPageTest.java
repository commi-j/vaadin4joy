package tk.labyrinth.vaadin4joy.opertv.front.page;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import tk.labyrinth.vaadin4joy.opertv.back.catalogue.VideoCatalogueImpl;

@ExtendWith(SpringExtension.class)
@Import({
		OperTvPage.class,
		VideoCatalogueImpl.class
})
@SpringJUnitConfig(OperTvPageTest.class)
class OperTvPageTest {

	@Autowired
	private OperTvPage page;

	@Test
	void testSetParameter() {
		// Test no failures
		page.setParameter(null, "");
		page.setParameter(null, "12");
		page.setParameter(null, "12/14");
		page.setParameter(null, "qwe");
		page.setParameter(null, "qwe/aze");
	}
}
